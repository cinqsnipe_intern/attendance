<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Route::group(['middlewareGroup' => 'webGroup'],function(){

//    Route::get('/','LoginController@login')->name('login');
//    Route::get('/login','LoginController@login');
//    Route::post('/login','LoginController@dologin')->name('admin.login');
//    Route::get('/logout','LoginController@logout')->name('logout');
Auth::routes();


Route::group(['middleware'=>'auth'],function(){

    Route::get('/home', 'HomeController@index');
Route::resource('/home/users','UserController');
//Route::resources('/home/attendance','AttendanceController');
Route::resource('/home/companies','CompaniesController');
Route::resource('/home/attendance','AttendanceController');
Route::resource('/home/holidays','HolidaysController');
Route::resource('/home/roles','RoleController');
Route::resource('/home/notices','NoticeController');
Route::resource('/home/leaves','LeavesController');
Route::resource('/movements','MovementsController');
//Route::resource('/holidays','HolidaysController');

    });
//});