<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('/userinfo/{id}','UserController@api_show');
Route::get('/userinfo','UserController@api_index');
Route::get('/notice','NoticeController@notice');
Route::get('/notice/{id}','NoticeController@notice_show');
Route::get('holiday/{id}','HolidaysController@holiday_api');
//Route::post('/Getleave','LeaveController@getleave');