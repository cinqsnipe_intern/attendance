<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    //
    protected $fillable=[
        'company_name','location','ssid_number','check_in','check_out','weekly_off','company_logo','no_of_employees','company_type'
    ]
    ;
}
