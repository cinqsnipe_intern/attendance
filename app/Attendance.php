<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    //
    protected $fillable=[
        'date','check_in','check_out','user_id'
        ];
    public function user(){
        return $this->hasMany('App\User','the_user_id');
    }
}


