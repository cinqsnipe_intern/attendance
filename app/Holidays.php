<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Holidays extends Model
{
    //
    protected $fillable=[
    'start_date','title','description','image','end_date'
    ];
}
