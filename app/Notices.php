<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notices extends Model
{
    //
    protected $fillable=[
        'image','title','date','location','description'
    ];
}
