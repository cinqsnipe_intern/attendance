<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notices;
use DB;

class NoticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $send=Notices::all();
        return view('notice.index',compact('send'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('notice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input=$request->all();
        if ($file=$request->file('image')){
            $name=$file->getClientOriginalName();
            $file->move('image',$name);
            $input['image']=$name;
        }
        Notices::create($input);


        return redirect('/home/notices');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function notice(){
        $send=Notices::all();
        return response()->json($send);

    }
    public function notice_show($id)
    {
        $noticeId=$id;
        $notice = DB::table('notices')
            ->select('id','title','description','date','image','location')
            ->where('id',$noticeId)
            ->get();
        if(is_null($notice))
        {
            $data = [
                "StatusCode" => 200,
                "Success" =>false,
                "Message"=>'No notice Found'
            ];
            return response($data,200);
        }else {
            $response = [
                "StatusCode" => 200,
                "Success" => true,
                "Message" => 'Notice Details Found',
                'notice detail' => $notice,
            ];
            return $response;
        }
    }
}
