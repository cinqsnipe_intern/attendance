<?php

namespace App\Http\Controllers;

use App\Holidays;
Use DB;
use Illuminate\Http\Request;

class HolidaysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $send=Holidays::all();
        return view('holidays.index',compact('send'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('holidays.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input=$request->all();
        if ($file=$request->file('image')){
            $name=$file->getClientOriginalName();
            $file->move('image',$name);
            $input['image']=$name;
        }
        Holidays::create($input);
        return redirect('/home/holidays');

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $holidays=Holidays::findorfail($id);
        return view('holidays.edit',compact('holidays'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $holidays=Holidays::findorfail($id);
        $holidays->update($request->all());
        return redirect('/home/holidays');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $holidays=Holidays::findorfail($id);
        $holidays->delete();
        return redirect('/home/holidays');
    }
    public function holiday_api($id){
        $holidayid=$id;
        if($holidayid==null){
            $data = [
                "StatusCode" => 200,
                "Success" =>false,
                "Message"=>'No Such User Found'
            ];
            return response()->json($data);
        }else {
            $data = DB::table('holidays')
                ->select('start_date', 'end_date', 'image', 'title', 'description')
                ->where('id', $holidayid)
                ->get();
            return response($data);
        }
    }
}
