<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *$send=User::all();
    return response()->json($send);
     * @return \Illuminate\Http\Response
     */
    public function api_index()
    {
        //
        $send=User::all();
        return response()->json($send);
//
    }
    public function index()
    {
        //
        $send=User::all();

        return view('users.index',compact('send'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input=$request->all();
        if ($file=$request->file('user_image')){
            $name=$file->getClientOriginalName();
            $file->move('image',$name);
            $input['user_image']=$name;
        }
        User::create($input);


        return redirect('/home/users');

        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function api_show($id)
    {
        $userId=$id;
        $users = DB::table('users')
            ->select('id','name','email','user_image','email','gender','skills','previous_company','start_date','phone_number')
            ->where('id',$userId)
            ->get();
        if(is_null($users))
        {
            $data = [
                "StatusCode" => 200,
                "Success" =>false,
                "Message"=>'No Such User Found'
            ];
            return response($data,200);
        }else {
            $response = [
                "StatusCode" => 200,
                "Success" => true,
                "Message" => 'User Details Found',
                'user-detail' => $users,
            ];
            return $response;
        }
    }
//    public function show($id)
//    {
//        //
//        $send=User::findorfail($id);
//        return response()->json($send);
//
//    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user=User::findorfail($id);
        return view('users.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user=User::findorfail($id);
        $user->update($request->all());
        return redirect('home/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user=User::findorfail($id);
        $user->delete();
        return redirect('home/users');
    }
}
