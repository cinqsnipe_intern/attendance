<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'user_id','name','address','experience','email', 'password','gender','user_image',
        'date_of_birth','position','role_id','is_active','phone_number','skills','start_date','documents','previous_company'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
//    public function attendance(){
//        return $this->belongsTo('App\Attendance');
//    }
}
