<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movements extends Model
{
    //
    protected $fillable=[
        'title','reason','out','in'
    ];
}
