<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leaves extends Model
{
    protected $fillable=[
        'title','description','start_date','end_date','deny_reason'
    ];
    //
}
