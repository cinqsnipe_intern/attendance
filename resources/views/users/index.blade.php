@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

<h1> Users Info </h1>
<h2><a href="{{route('users.create')}}">Add Users</a></h2>


    <table class="table-striped table-bordered table-condensed">
        <th>Employee_id</th>
        <th>Email</th>


        <th>Name</th>
        <th>role</th>
        <th>Date Of Birth</th>
        <th>Gender</th>
        <th>Image</th>
        <th>Status</th>
        <th>Position</th>
        <th>Created At</th>

        @foreach($send as $a)
        <tr>
            <td>{{$a->user_id}}</td>
            <td>{{$a->email}}</td>


            <td>{{$a->name}}</td>
            <td>{{$a->role_id}}</td>
            <td>{{$a->date_of_birth}}</td>
            <td>{{$a->gender}}</td>
            <td><div class="tbimg"><img src="/image/{{$a->user_image}}"></div></td>
            <td>{{$a->is_active}}</td>
            <td>{{$a->position}}</td>
            <td>{{$a->created_at}}</td>
            <td><a href="{{route('users.edit',$a->id)}}"><button class="btn btn-primary">Edit</button></a></td>
            {{--<td><a href="{{route('users.edit',$a->id)}}"><button class="btn btn-danger">Delete</button></a></td>--}}

        </tr>
        @endforeach
    </table>
                </div>
            </div>
        </div>


@endsection