@extends('layouts.app')
@section('title')
Add User
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">

<form class="form-horizontal" method="post" enctype="multipart/form-data" action="/home/users">
  {{csrf_field()}}
	<div>Add</div>


    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Name</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="name" placeholder="Name">
        </div>
    </div>


  <div class="form-group">

    <label for="email" class="col-sm-2 control-label">Email</label>
    <div class="col-sm-10">
      <input type="email" class="form-control" name="email" placeholder="Email">
    </div>
  </div>

  <div class="form-group">
    <label for="password" class="col-sm-2 control-label">Password</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" name="password" placeholder="Password">
    </div>
  </div>
    {{--<div class="form-group">--}}


        {{--<label for="address" class="col-sm-2 control-label"> Address</label>--}}
        {{--<div class="col-sm-10">--}}
            {{--<input type="text" class="form-control" name="address" placeholder="address">--}}
        {{--</div>--}}
    {{--</div>--}}

 <div class="form-group">
    <label for="gender" class="col-sm-2 control-label">Gender</label>
    <div class="col-sm-10">
      <select name="gender" class="form-control">
  <option>Male</option>
  <option>Female</option>

</select>
</div>

  </div>
  <div class="form-group">
    <label for="date_of_birth" class="col-sm-2 control-label">Date of Birth</label>
    <div class="col-sm-10" >
      <input type="date" class="form-control" name="date_of_birth" placeholder="Date of Birth">
    </div>
  </div>


<div class="form-group">
    <label for="user_image" class="col-sm-2 control-label">User Image</label>
    <div class="col-sm-10">
      <input type="file" class="form-control" name="user_image" >
    </div>
  </div>
<div class="form-group">
    <label for="documents" class="col-sm-2 control-label">Documents</label>
    <div class="col-sm-10">
      <input type="file" class="form-control" name="documents" multiple >
    </div>
  </div>

<div class="form-group">
    <label for="start_date" class="col-sm-2 control-label">Joined Date</label>
    <div class="col-sm-10">
      <input type="date" class="form-control" name="start_date" >
    </div>
  </div>
<div class="form-group">
    <label for="experience" class="col-sm-2 control-label"> Experience</label>
    <div class="col-sm-10">
      <input type="number" class="form-control" name="experience" placeholder="experience" >
    </div>
  </div>
<div class="form-group">
    <label for="skills" class="col-sm-2 control-label">Skills</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="skills" placeholder="skills">
    </div>
  </div>
<div class="form-group">
    <label for="previous_company" class="col-sm-2 control-label">Previous Company</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="previous_company" placeholder="Previous company" >
    </div>
  </div>
<div class="form-group">
    <label for="position" class="col-sm-2 control-label">Position</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="Position" placeholder="Position" >
    </div>
  </div>

<div class="form-group">
    <label for="is_active" class="col-sm-2 control-label">Status</label>
    <div class="col-sm-10">
      <select name="is_active" class="form-control">
  <option>Active</option>
  <option>Inactive</option>

</select>
</div>

  </div>



    <div class="col-sm-offset-2 col-sm-10">
      <input type="submit" name="submit" class="btn btn-primary">
    </div>

</form>
        </div>
      </div>
    </div>

{{--<form method="post" action="/users">--}}
  {{--{{csrf_field()}}--}}
  {{--<input type="text" name="title" placeholder="title">--}}
  {{--<input type="submit" name="submit">--}}
{{--</form>--}}
@endsection