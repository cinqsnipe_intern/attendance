@extends('layouts.app')
@section('title')
    Add User
@endsection

@section('content')

        <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

    <form class="form-horizontal" method="post" action="/home/users/{{$user->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
        <div>Edit</div>

        <div class="form-group">
            {{--<label for="id" class="col-sm-2 control-label">id</label>--}}
            {{--<div class="col-sm-10">
              <input type="id" class="form-control" id="id" placeholder="id">
            </div>
          </div>--}}

            {{--<div class="form-group">--}}
                {{--<label for="id" class="col-sm-2 control-label">User_id</label>--}}
                {{--<div class="col-sm-10">--}}
                    {{--<input type="text" class="form-control" name="id" value="{{$user->user_id}}" placeholder="Employee_id">--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="form-group">
                <label for="email" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control"value="{{$user->email}}" name="email" placeholder="Email">
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                    <input type="name" class="form-control" value="{{$user->name}}" name="name" placeholder="name">
                </div>
            </div>
            {{--<div class="form-group">--}}
                {{--<label for="password" class="col-sm-2 control-label">Password</label>--}}
                {{--<div class="col-sm-10">--}}
                    {{--<input type="password" class="form-control" value="{{$user->password}}" name="password" placeholder="Password">--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="form-group">--}}
                {{--<label for="first_name" class="col-sm-2 control-label">First_name</label>--}}
                {{--<div class="col-sm-10">--}}
                    {{--<input type="text" class="form-control" value="{{$user->first_name}}" name="first_name" placeholder="first_name">--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="form-group">--}}
                {{--<label for="middle_name" class="col-sm-2 control-label">Middle_name</label>--}}
                {{--<div class="col-sm-10">--}}
                    {{--<input type="text" class="form-control" value="{{$user->middle_name}}" name="middle_name" placeholder="middle_name">--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="form-group">--}}
                {{--<label for="last_name" class="col-sm-2 control-label">Last_name</label>--}}
                {{--<div class="col-sm-10">--}}
                    {{--<input type="text" class="form-control" value="{{$user->last_name}}" name="last_name" placeholder="last_name">--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="form-group">
                <label for="gender" class="col-sm-2 control-label">Gender</label>
                <div class="col-sm-10">
                    <select name="gender" value="gender" class="form-control">
                        <option>Male</option>
                        <option>Female</option>

                    </select>
                </div>

            </div>
            <div class="form-group">
                <label for="date_of_birth" class="col-sm-2 control-label">Date of Birth</label>
                <div class="col-sm-10">
                    <input type="date" class="form-control" value="{{$user->date_of_birth}}"name="date_of_birth" placeholder="Date of Birth">
                </div>
            </div>


            {{--<div class="form-group">--}}
                {{--<label for="position" class="col-sm-2 control-label">Position</label>--}}
                {{--<div class="col-sm-10">--}}
                    {{--<input type="text" class="form-control" name="position" value="{{$user->position}}" placeholder="Position">--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="form-group">--}}
                {{--<label for="role" class="col-sm-2 control-label">Role</label>--}}
                {{--<div class="col-sm-10"><select name="role"value="{{$user->role}}" class="form-control">--}}
                        {{--<option>Admin</option>--}}
                        {{--<option>Employee</option>--}}

                    {{--</select>--}}
                {{--</div>--}}

            {{--</div>--}}
            <div class="form-group">
                <label for="is_active" class="col-sm-2 control-label">Status</label>
                <div class="col-sm-10">
                    <select name="is_active" value="{{$user->is_active}}" class="form-control">
                        <option>Active</option>
                        <option>Inactive</option>

                    </select>
                </div>

            </div>


            <div class="row">
            <div class="col-sm-offset-2 col-sm-10">
                <input type="submit" name="submit" value="Update" class="btn btn-primary">
            </div>

    </form>

            </div>
            <div class="col-sm-offset-2 col-sm-2"></div>

            <form method="post"  action="/home/users/{{$user->id}}">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="DELETE">
                <input class="btn btn-danger" type="submit" value="Delete">
            </form>
        </div>

        </div>


            </div>
        </div>



@endsection