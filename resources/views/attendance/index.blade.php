@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1>Attendance Info</h1>
                <a href="{{route('attendance.create')}}">Add attendance</a>
                <table class="table-striped table-bordered table-condensed">
                    <th>Date</th>
                    <th>Check_in</th>
                    <th>Check_out</th>
                    @foreach($attendance as $a)
                        <tr>
                            <td>{{$a->date}}</td>
                            <td>{{$a->check_in}}</td>
                            <td>{{$a->check_out}}</td>


                        </tr>
                        @endforeach
                </table>
            </div>
        </div>
    </div>
    @endsection