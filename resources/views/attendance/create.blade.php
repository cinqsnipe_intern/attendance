@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form class="form-horizontal" method="post" action="/home/attendance">
                    {{csrf_field()}}



                    <div class="form-group">
                        <label for="date" class="col-sm-2 control-label">Company Name</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" name="date" placeholder="company_name">
                        </div>
                    </div>
                    <div class="form-group">


                        <label for="check_in" class="col-sm-2 control-label">Check In</label>
                        <div class="col-sm-10">
                            <input type="time" class="form-control" name="check_in" placeholder="check_in">
                        </div>
                    </div>
                    <div class="form-group">


                        <label for="check_out" class="col-sm-2 control-label">Check Out</label>
                        <div class="col-sm-10">
                            <input type="time" class="form-control" name="check_out" placeholder="check_in">
                        </div>
                    </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <input type="submit" name="submit" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endsection