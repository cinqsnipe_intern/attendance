@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <h1> Edit Holidays </h1>
                <form class="form-horizontal" method="post" action="/home/holidays/{{$holidays->id}}">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group">


                        <label for="title" class="col-sm-2 control-label">Role Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="title" value="{{$holidays->title}}" placeholder="title">
                        </div>
                        <div class="form-group">


                        <label for="title" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="title" value="{{$holidays->description}}" placeholder="title">
                        </div>
                    </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <input type="submit" value="Update" name="submit" class="btn btn-primary">
                    </div>

                </form>
                <form method="post"  action="/home/roles/{{$roles->id}}">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <input class="btn btn-danger" type="submit" value="Delete">
                </form>
            </div>
        </div>
    </div>
@endsection