@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <h1>Holidays Info</h1>
                <a href="{{route('holidays.create')}}">Add Holidays</a>
                <table class="table-striped table-bordered table-condensed">
                    <th>Title</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Image</th>



                    @foreach($send as $a)
                        <tr>
                            <td>{{$a->title}}</td>
                            <td>{{$a->start_date}}</td>
                            <td>{{$a->end_date}}</td>
                            <td>{{$a->title}}</td>
                            <td>{{$a->description}}</td>
                            <td><div class="tbimg"><img src="/image/{{$a->image}}"></div></td>
                            <td><a href="{{route('holidays.edit',$a->id)}}"><button class="btn btn-primary">Edit</button></a></td>

                        </tr>
                        @endforeach
                </table>
            </div>
        </div>
    </div>
                @endsection