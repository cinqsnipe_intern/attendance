@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">


                <form class="form-horizontal" enctype="multipart/form-data" method="post" action="/home/holidays">
                    {{csrf_field()}}
                    <div>Add</div>
                    <div class="form-group">
                        <label for="id" class="col-sm-2 control-label">Title</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="id" name="title" placeholder="id">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="start_date" class="col-sm-2 control-label">Start Date</label>
                        <div class="col-sm-10">
                          <input type="date" class="form-control"  name="start_date" placeholder="date">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="end_date" class="col-sm-2 control-label">End Date</label>
                        <div class="col-sm-10">
                          <input type="date" class="form-control"  name="end_date" placeholder="date">
                        </div>
                    </div>

                  <div class="form-group">
                        <label for="description" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control"  name="description" placeholder="description">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="image" class="col-sm-2 control-label">Image</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control" name="image" >
                        </div>
                    </div>

                    <div class="col-sm-offset-2 col-sm-10">
                        <input type="submit" name="submit" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>

    @endsection