@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <h1> Edit Companies </h1>
                <form class="form-horizontal" method="post" action="/home/companies/{{$companies->id}}">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="PUT">
                <div class="form-group">


                    <label for="company_name" class="col-sm-2 control-label">Company Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="company_name" value="{{$companies->company_name}}" placeholder="company_name">
                    </div>
                </div>

                <div class="form-group">

                    <label for="location" class="col-sm-2 control-label">Company Location</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="location" value="{{$companies->location}}" placeholder="company_location">
                    </div>
                </div>

                <div class="form-group">
                    <label for="ssid_number" class="col-sm-2 control-label">Ssid Number</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="ssid_number"value="{{$companies->ssid_number}}" placeholder="ssid_number">
                    </div>
                </div>
                <div class="form-group">
                    <label for="check_in" class="col-sm-2 control-label">Check_in</label>
                    <div class="col-sm-10">
                        <input type="time" class="form-control" value="{{$companies->check_in}}" name="check_in" placeholder="check_in">
                    </div>
                </div>
                <div class="form-group">
                    <label for="check_out" class="col-sm-2 control-label">Check_out</label>
                    <div class="col-sm-10">
                        <input type="time" class="form-control" value="{{$companies->check_out}}" name="check_out" placeholder="check_out">
                    </div>
                </div>


                <div class="form-group">
                    <label for="weekly_off" class="col-sm-2 control-label">Weekly off</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" value="{{$companies->weekly_off}}" name="weekly_off" placeholder="weekly_off">
                    </div>
                </div>


                {{--<div class="form-group">
                    <label for="position" class="col-sm-2 control-label">Position</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="position" placeholder="Position">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="role" class="col-sm-2 control-label">Role</label>
                    <div class="col-sm-10"><select name="role" class="form-control">
                  <option>Admin</option>
                  <option>Employee</option>--}}

                {{--</select>--}}
                {{--</div>--}}

                {{--</div>--}}



                <div class="col-sm-offset-2 col-sm-10">
                    <input type="submit" value="Update" name="submit" class="btn btn-primary">
                </div>

                </form>
                <form method="post"  action="/home/companies/{{$companies->id}}">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <input class="btn btn-danger" type="submit" value="Delete">
                </form>
            </div>
        </div>
    </div>
@endsection