@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <h1>Companies Info</h1>
                <a href="{{route('companies.create')}}">Add Companies</a>
                <table class="table-striped table-bordered table-condensed">
                <th>Company_id</th>
                <th>Company Name</th>
                <th>Location</th>


                <th>Ssid_number</th>
                <th>Check in</th>
                <th>Check out</th>
                <th>Weekly off</th>

                <th>Created At</th>
                    <th>Updated At</th>
                    @foreach($companies as $a)
                        <tr>
                            <td>{{$a->id}}</td>
                            <td>{{$a->company_name}}</td>
                            <td>{{$a->location}}</td>


                            <td>{{$a->ssid_number}}</td>
                            <td>{{$a->check_in}}</td>
                            <td>{{$a->check_out}}</td>
                            <td>{{$a->weekly_off}}</td>
                            <td>{{$a->created_at}}</td>
                            <td>{{$a->updated_at}}</td>
                            {{--<td>{{$a->created_at}}</td>--}}
                            <td><a href="{{route('companies.edit',$a->id)}}"><button class="btn btn-primary">Edit</button></a></td>
               @endforeach
                </table>

            </div>
        </div>
    </div>
@endsection