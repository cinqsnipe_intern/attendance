@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <table class="table-bordered">
                    <th>title</th>
                    <th>Reason</th>
                    <th>Out</th>
                    <th>In</th>
                    @foreach($send as $a)
                        <tr>
                            <td>{{$a->title}}</td>
                            <td>{{$a->reason}}</td>
                            <td>{{$a->out}}</td>
                            <td>{{$a->in}}</td>
                        </tr>
                    @endforeach
                </table>

            </div>
        </div>
    </div>
    @endsection