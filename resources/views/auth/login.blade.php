@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
{{--@endsection--}}
{{--<!DOCTYPE html>--}}
{{--<html>--}}
{{--<head>--}}
    {{--<!-- Styles -->--}}
    {{--<link href="/css/app.css" rel="stylesheet">--}}
    {{--<meta charset="UTF-8">--}}
    {{--<title>AdminLTE 2 | Dashboard</title>--}}
    {{--<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>--}}
    {{--<!-- Bootstrap 3.3.2 -->--}}
    {{--<link href="{{ asset("/adminlte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />--}}
    {{--<!-- Font Awesome Icons -->--}}
    {{--<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />--}}
    {{--<!-- Ionicons -->--}}
    {{--<link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />--}}
    {{--<!-- Theme style -->--}}
    {{--<link href="{{ asset("/adminlte/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />--}}
    {{--<!-- AdminLTE Skins. We have chosen the skin-blue for this starter--}}
          {{--page. However, you can choose any other skin. Make sure you--}}
          {{--apply the skin class to the body tag so the changes take effect.--}}
    {{---->--}}
    {{--<link href="{{ asset("/adminlte/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />--}}

    {{--<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->--}}
    {{--<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->--}}
    {{--<!--[if lt IE 9]>--}}
    {{--<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>--}}
    {{--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>--}}
    {{--<![endif]-->--}}
{{--</head>--}}
{{--<body class="hold-transition login-page">--}}
{{--<div class="login-box">--}}
    {{--<div class="login-logo">--}}
        {{--<a href="../../index2.html"><b>Admin</b>LTE</a>--}}
    {{--</div>--}}
    {{--<!-- /.login-logo -->--}}
    {{--<div class="login-box-body">--}}
        {{--<p class="login-box-msg">Sign in to start your session</p>--}}

        {{--<form action="../../index2.html" method="post">--}}
            {{--<div class="form-group has-feedback">--}}
                {{--<input type="email" class="form-control" placeholder="Email">--}}
                {{--<span class="glyphicon glyphicon-envelope form-control-feedback"></span>--}}
            {{--</div>--}}
            {{--<div class="form-group has-feedback">--}}
                {{--<input type="password" class="form-control" placeholder="Password">--}}
                {{--<span class="glyphicon glyphicon-lock form-control-feedback"></span>--}}
            {{--</div>--}}
            {{--<div class="row">--}}
                {{--<div class="col-xs-8">--}}
                    {{--<div class="checkbox icheck">--}}
                        {{--<label>--}}
                            {{--<input type="checkbox"> Remember Me--}}
                        {{--</label>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<!-- /.col -->--}}
                {{--<div class="col-xs-4">--}}
                    {{--<button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>--}}
                {{--</div>--}}
                {{--<!-- /.col -->--}}
            {{--</div>--}}
        {{--</form>--}}

        {{--<div class="social-auth-links text-center">--}}
            {{--<p>- OR -</p>--}}
            {{--<a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using--}}
                {{--Facebook</a>--}}
            {{--<a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using--}}
                {{--Google+</a>--}}
        {{--</div>--}}
        {{--<!-- /.social-auth-links -->--}}

        {{--<a href="#">I forgot my password</a><br>--}}
        {{--<a href="register.html" class="text-center">Register a new membership</a>--}}

    {{--</div>--}}
    {{--<!-- /.login-box-body -->--}}
{{--</div>--}}
{{--<!-- /.login-box -->--}}

{{--<!-- jQuery 2.2.3 -->--}}
{{--<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>--}}
{{--<!-- Bootstrap 3.3.6 -->--}}
{{--<script src="../../bootstrap/js/bootstrap.min.js"></script>--}}
{{--<!-- iCheck -->--}}
{{--<script src="../../plugins/iCheck/icheck.min.js"></script>--}}
{{--<script>--}}
    {{--$(function () {--}}
        {{--$('input').iCheck({--}}
            {{--checkboxClass: 'icheckbox_square-blue',--}}
            {{--radioClass: 'iradio_square-blue',--}}
            {{--increaseArea: '20%' // optional--}}
        {{--});--}}
    {{--});--}}
{{--</script>--}}
{{--</body>--}}
{{--</html>--}}