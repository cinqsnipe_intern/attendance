@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form class="form-horizontal" method="post" action="/home/leaves">
                    {{csrf_field()}}



                    <div class="form-group">


                        <label for="title" class="col-sm-2 control-label">Title</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="title" placeholder="title">
                        </div>
                    </div>
                    <div class="form-group">


                        <label for="description" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="description" placeholder="Description">
                        </div>
                    </div>
                    <div class="form-group">


                        <label for="start_date" class="col-sm-2 control-label">Start Date</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" name="start_date" placeholder="Description">
                        </div>
                    </div>
                    <label for="end_date" class="col-sm-2 control-label">End Date</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" name="end_date" placeholder="Description">
                        </div>
                    </div>

                    <div class="col-sm-offset-2 col-sm-10">
                        <input type="submit" name="submit" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endsection