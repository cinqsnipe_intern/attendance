@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1> Edit Holidays </h1>
                <form class="form-horizontal" method="post" action="/home/leaves/{{$leaves->id}}">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group">


                        <label for="title" class="col-sm-2 control-label">Role Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="title" value="{{$leaves->title}}" placeholder="title">
                        </div>
                        <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="title" value="{{$leaves->description}}" placeholder="title">
                        </div>

                        <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Start Date</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" name="start_date" value="{{$leaves->start_date}}" placeholder="title">
                        </div>

                        <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">End Date</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" name="end_date" value="{{$leaves->end_date}}" placeholder="title">
                        </div>

                    </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <input type="submit" value="Update" name="submit" class="btn btn-primary">
                    </div>

                </form>
                <form method="post"  action="/home/leaves/{{$leaves->id}}">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <input class="btn btn-danger" type="submit" value="Delete">
                </form>
            </div>
        </div>
    </div>
    @endsection