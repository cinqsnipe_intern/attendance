@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1>Leaves Info</h1>
                <a href="{{route('leaves.create')}}">Add Leaves</a>
                <table class="table-striped table-bordered table-condensed">
                    <th>Leave Id</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Start Date</th>
                    <th>End Date</th>



                    @foreach($send as $a)
                        <tr>
                            <td>{{$a->id}}</td>
                            <td>{{$a->title}}</td>
                            <td>{{$a->description}}</td>
                            <td>{{$a->start_date}}</td>
                            <td>{{$a->end_date}}</td>
                            <td><a href="{{route('leaves.edit',$a->id)}}"><button class="btn btn-primary">Edit</button></a></td>
                        </tr>
                        @endforeach
                </table>
            </div>
        </div>
    </div>
    @endsection