@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <h1>Add Roles</h1>
                <form class="form-horizontal" method="post" action="/home/roles">
                    {{csrf_field()}}


                    <div class="form-group">
                    <label for="role_name" class="col-sm-2 control-label">Role Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="role_name" placeholder="Role Name">
                    </div>
                </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <input type="submit" name="submit" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endsection