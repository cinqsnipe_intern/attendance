@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <h1> Roles Info </h1>
                <h2><a href="{{route('roles.create')}}">Add Role</a></h2>

                <table class="table-striped table-bordered table-condensed">
                    <th>Role_id</th>
                    <th>Role Name</th>
                    @foreach($roles as $a)
                        <tr>
                        <td>{{$a->id}}</td>
                        <td>{{$a->role_name}}</td>
                            <td><a href="{{route('roles.edit',$a->id)}}"><button class="btn btn-primary">Edit</button></a></td>
                        </tr>
                        @endforeach
                </table>
            </div>
        </div>
    </div>
                @endsection