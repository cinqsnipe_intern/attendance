@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <h1> Edit roles </h1>
                <form class="form-horizontal" method="post" action="/home/roles/{{$roles->id}}">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group">


                        <label for="role_name" class="col-sm-2 control-label">Role Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="role_name" value="{{$roles->role_name}}" placeholder="company_name">
                        </div>
                    </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <input type="submit" value="Update" name="submit" class="btn btn-primary">
                    </div>

                </form>
                <form method="post"  action="/home/roles/{{$roles->id}}">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <input class="btn btn-danger" type="submit" value="Delete">
                </form>
            </div>
        </div>
    </div>
@endsection