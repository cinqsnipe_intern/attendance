@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1><a href="{{route('notices.create')}}"> Add Notice</a></h1>
                <table class="table-striped table-bordered table-condensed">
                    <th>Title</th>
                    <th>Image</th>
                    <th>Date</th>
                    @foreach($send as $a)
                        <tr>
                            <td>{{$a->title}}</td>
                            <td><div class="tbimg"><img src="/image/{{$a->image}}"></div></td>
                            <td>{{$a->date}}</td>
                            <td><a href="{{route('notices.edit',$a->id)}}"><button class="btn btn-primary">Edit</button></a></td>
                        </tr>
                        @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection