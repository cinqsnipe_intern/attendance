<?php

use Illuminate\Database\Seeder;
use App\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user= new User;
        $user->name="Abin Shrestha";
        $user->email="abin@gmail.com";
        $user->password= Hash::make('admin');
        $user->skills="PHP, HTML, CSS, Laravel";
        $user->previous_company="None";
        $user->gender="Male";
        $user->position="Web Developer";
        $user->save();
    }
}
