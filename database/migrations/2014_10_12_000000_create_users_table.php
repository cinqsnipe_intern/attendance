<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id')->default('1');
            $table->string('is_active')->default('active');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->text('user_image')->nullable();
            $table->text('skills')->nullable();
            $table->text('previous_company')->nullable();
            $table->text('documents')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('gender')->nullable();
            $table->string('position')->nullable();
            $table->integer('experience')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->date('start_date')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
